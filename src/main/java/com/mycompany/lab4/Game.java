/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author informatics
 */
public class Game {

    private Player player1, player2;
    private Table table;

    public Game() {
        player1 = new Player('X');
        player2 = new Player('O');
    }

    public void play() {
        boolean isFinish = false;
        printWelcome();
        newGame();
        while (!isFinish) {
            printTable();
            printTurn();
            inputRowCol();
            if (table.checkWin()) {
                printTable();
                showWinner();
                printPlayers();
                isFinish = true;
            }
            if (table.checkDraw()) {
                printTable();
                showDraw();
                printPlayers();
                isFinish = true;
            }
            table.switchPlayer();
        }

    }

    private void printWelcome() {
        System.out.println("Welcome To OX Game");
    }

    private void printTable() {
        char[][] t = table.getTable();
        System.out.println("-------------");
        for (int i = 0; i < 3; i++) {
            System.out.print("| ");
            for (int j = 0; j < 3; j++) {
                System.out.print(t[i][j] + " | ");
            }
            System.out.println("");
        }
        System.out.println("-------------");

    }

    private void printTurn() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }

    private void inputRowCol() {
        Scanner kb = new Scanner(System.in);
        char[][] t = table.getTable();
        while (true) {
            System.out.print("Please input row col :");

            int row = kb.nextInt();
            int col = kb.nextInt();
            if (row >= 0 && row < 3 && col >= 0 && col < 3 && t[row][col] == '-') {
                table.setRowCol(row, col);
                return;
            }
        }

    }

    private void newGame() {
        table = new Table(player1, player2);
    }

    private void showWinner() {
        System.out.println(table.getCurrentPlayer().getSymbol() + " Win!!!");
    }

    private void showDraw() {
        System.out.println("Draw!!!");
    }

    private void printPlayers() {
        System.out.println(player1);
        System.out.println(player2);
    }

}
